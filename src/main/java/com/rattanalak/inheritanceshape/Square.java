/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritanceshape;

/**
 *
 * @author Rattanalak
 */
public class Square extends Rectangle{
    public Square(double side){
        super(side,side);
    }
    @Override
    public void ShowArea(){
        System.out.println("Area of the square (wide =" + wide + ") is " + this.calArea());
    }
    
}
