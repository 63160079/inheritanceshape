/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritanceshape;

/**
 *
 * @author Rattanalak
 */
public class Rectangle extends Shape{
    protected double wide;
    protected double length;
    
    public Rectangle(double wide, double length){
        if (wide <= 0 || length <=0){
            System.out.println("Error: wide and length must more than zero!!!");
            return;
        }
        else if(wide >= length){
            System.out.println("Error: wide must less than length");
        }
        this.length = length;
        this.wide = wide;
    }
    @Override
    public double calArea(){
        return wide * length;
    }
    @Override
     public void ShowArea(){
        System.out.println("Area of the rectangle (wide = " + this.wide + ", length = " + this.length + ") is " + this.calArea());
    }
}
