/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritanceshape;

/**
 *
 * @author Rattanalak
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape1 = new Shape();
        shape1.calArea();
        shape1.ShowArea();
        Circle circle1 = new Circle(3);
        circle1.calArea();
        circle1.ShowArea();
        Circle circle2 = new Circle(4);
        circle2.calArea();
        circle2.ShowArea();
        Triangle triangle1 = new Triangle(3,4);
        triangle1.calArea();
        triangle1.ShowArea();
        Rectangle rectangle1 = new Rectangle(3,4);
        rectangle1.calArea();
        rectangle1.ShowArea();
        Square square1 = new Square(2);
        square1.calArea();
        square1.ShowArea();
    }
   
    
            
            
            
}
