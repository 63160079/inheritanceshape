/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritanceshape;

/**
 *
 * @author Rattanalak
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;
    
    public Circle(double r){
        if (r<=0){
             System.out.println("Error: Radius must more than zero!!!");
             return;
        }
        this.r = r;
        
    }
    
    
    @Override
    public double calArea(){
        return pi*r*r;
    }
    
    @Override
    public void ShowArea(){
        System.out.println("Area of circle (r ="+ this.r +") is "+ this.calArea());
    }
}
