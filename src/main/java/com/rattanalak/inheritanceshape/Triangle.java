/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritanceshape;

/**
 *
 * @author Rattanalak
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        if (base <= 0 || height <= 0){
            System.out.println("Error: Base and Heigth must more than zero!!!");
            return;
        }
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return 0.5 * base*height;
    }
    @Override
     public void ShowArea(){
        System.out.println("Area of the tritangle (base = " + this.base+ ", heigth = " + this.height+ ") is " + this.calArea());
    }

}
